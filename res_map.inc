<?php
/**
 * Migrate v2 Map class for Migrate Remote RES.
 *
 * @copyright (c) 2011 OSInet
 *
 * @author Frédéric G. MARAND (fgm@osinet.fr)
 *
 * Licensed under the General Public License version 2 or later.
 */

class ResMigrateMap extends MigrateMap {

  public function __construct() {
    // No parent constructor
      watchdog('res', '@method(@args)', _res_get_caller(1), WATCHDOG_DEBUG);
  }

  /**
   * Clear all messages from the map.
   */
  public function clearMessages() {
      watchdog('res', '@method(@args)', _res_get_caller(1), WATCHDOG_DEBUG);
  }

  /**
   * Delete the map and message entries for a given source record
   *
   * @param array $source_key
   */
  public function delete(array $source_key, $messages_only = FALSE) {
    watchdog('res', '@method(@args)', _res_get_caller(1), WATCHDOG_DEBUG);
    if (empty($source_key)) {
      watchdog('res', __CLASS__ .'::'. __FUNCTION__ . "(empty source key)\n", array(), WATCHDOG_ERROR);
    }
  }

  /**
   * Delete the map and message entries for a given destination record
   *
   * @param array $destination_key
   */
  public function deleteDestination(array $destination_key) {
      watchdog('res', '@method(@args)', _res_get_caller(1), WATCHDOG_DEBUG);
  }

  /**
   * Delete the map and message entries for a set of given source records.
   *
   * @param array $source_keys
   */
  public function deleteBulk(array $source_keys) {
      watchdog('res', '@method(@args)', _res_get_caller(1), WATCHDOG_DEBUG);
  }

  /**
   * Remove any persistent storage used by this map (e.g., map and message tables)
   */
  public function destroy() {
      //watchdog('res', '@method(@args)', _res_get_caller(1), WATCHDOG_DEBUG);
  }

  /**
   * Report the number of items that failed to import
   */
  public function errorCount() {
      watchdog('res', '@method(@args)', _res_get_caller(1), WATCHDOG_DEBUG);
  }

  /**
   * Arrays of key fields for the source and destination. Array keys are the
   * field names - array values are specific to the concrete map class.
   *
   * @var array
   */
  public function getDestinationKey() {
      watchdog('res', '@method(@args)', _res_get_caller(1), WATCHDOG_DEBUG);
    $ret = array();
    return $ret;
  }

  /**
   * Retrieve map data for a given source item
   */
  public function getRowBySource(array $source_id) {
      watchdog('res', '@method(@args)', _res_get_caller(1), WATCHDOG_DEBUG);
  }

  /**
   * Retrieve map data for a given destination item
   */
  public function getRowByDestination(array $destination_id) {
      watchdog('res', '@method(@args)', _res_get_caller(1), WATCHDOG_DEBUG);
  }


  public function getSourceKey() {
      watchdog('res', '@method(@args)', _res_get_caller(1), WATCHDOG_DEBUG);
    $ret = array();
    return $ret;
  }

  /**
   * Report the number of imported items in the map
   */
  public function importedCount() {
      watchdog('res', '@method(@args)', _res_get_caller(1), WATCHDOG_DEBUG);
  }

  /**
   * Report the number of messages
   */
  public function messageCount() {
      watchdog('res', '@method(@args)', _res_get_caller(1), WATCHDOG_DEBUG);
  }

  /**
   * Prepare to run a full update - mark all previously-imported content as
   * ready to be re-imported.
   */
  public function prepareUpdate() {
      watchdog('res', '@method(@args)', _res_get_caller(1), WATCHDOG_DEBUG);
  }

  /**
   * Save a mapping from the key values in the source row to the destination
   * keys.
   *
   * @param $source_row
   * @param $dest_ids
   * @param $needs_update
   */
  public function saveIDMapping(stdClass $source_row, array $dest_ids, $needs_update = FALSE) {
      watchdog('res', '@method(@args)', _res_get_caller(1), WATCHDOG_DEBUG);
  }

  /**
   * Record a message related to a source record
   *
   * @param array $source_key
   *  Source ID of the record in error
   * @param string $message
   *  The message to record.
   * @param int $level
   *  Optional message severity (defaults to MESSAGE_ERROR).
   */
  public function saveMessage($source_key, $message, $level = MigrationBase::MESSAGE_ERROR) {
      watchdog('res', '@method(@args)', _res_get_caller(1), WATCHDOG_DEBUG);

  }

  // ======== Iterator interface ===============================================
  /**
   * (non-PHPdoc)
   * @see Iterator::current()
   */
  public function current() {
      watchdog('res', '@method(@args)', _res_get_caller(1), WATCHDOG_DEBUG);
  }

  /**
   * (non-PHPdoc)
   * @see Iterator::key()
   */
  public function key() {
      watchdog('res', '@method(@args)', _res_get_caller(1), WATCHDOG_DEBUG);
  }

  /**
   * (non-PHPdoc)
   * @see Iterator::next()
   */
  public function next() {
      watchdog('res', '@method(@args)', _res_get_caller(1), WATCHDOG_DEBUG);
  }

  /**
   * (non-PHPdoc)
   * @see Iterator::rewind()
   */
  public function rewind() {
      watchdog('res', '@method(@args)', _res_get_caller(1), WATCHDOG_DEBUG);
  }

  /**
   * (non-PHPdoc)
   * @see Iterator::valid()
   */
  public function valid() {
      watchdog('res', '@method(@args)', _res_get_caller(1), WATCHDOG_DEBUG);
  }
}

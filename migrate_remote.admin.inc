<?php

function migrate_remote_page_client($client) {
  $bc = _res_get_base_breadcrumb();
  $bc[] = l(t('Migrate server'), 'admin/content/migrate_remote/overview');
  drupal_set_breadcrumb($bc);
  if ($client['status'] == 0) {
    $ret = array(
      '#markup' => t('Client at !link is not currently allowed to use RES client migrations with <a href="!server">this server</a>.', array(
        '!link' => l($client['base'], filter_xss_admin($client['base']), array('absolute' => TRUE)),
        '!server' => url('admin/content/migrate_remote/overview'),
      ))
    );
  }
  else {
    $sts = xmlrpc($client['endpoint'], array ('migrateRemote.getMigrationStats' => array(array())));
    if (!$sts) {
      $message = xmlrpc_error_msg();
      $ret = array(
        '#markup' => t('Web service error: @message', array('@message' => $message)),
      );
    }
    else {
      //dsm($sts);
      // Client migration information
      $header = array(
        t('Status'),
        t('Migration'),
        t('Total rows'),
        t('Imported'),
        t('Unimported'),
        t('Messages'),
        t('Throughput'),
        t('Last imported'),
      );
      $ret = array(
        'migrations' => array(
          '#theme' => 'table',
          '#header' => $header,
          '#rows' => array(),
          '#caption' => drupal_render($sts['overview']),
      )
      );
      $migration_labels = array(
        MigrationBase::STATUS_IDLE         => t('Idle'),
        MigrationBase::STATUS_IMPORTING    => t('Importing'),
        MigrationBase::STATUS_ROLLING_BACK => t('Rolling back'),
        MigrationBase::STATUS_STOPPING     => t('Stopping'),
        MigrationBase::STATUS_DISABLED     => t('Disabled'),
      );
      foreach ($sts['rows'] as $migration) {
        $row = array(
          $migration_labels[$migration['status']],
          $migration['machinenamelink'],
          $migration['importrows'],
          $migration['imported'],
          $migration['unimported'],
          empty($migration['messageslink'])
            ? $migration['messages']
            : $migration['messageslink'],
          $migration['lastthroughput'],
          $migration['lastimported'],
        );
        $ret['migrations']['#rows'][] = $row;
      }

      // Client queue information
      $queue_status = empty($sts['queuecount'])
        ? t('Operations queue on @base is empty.', array('@base' => $client['base']))
        : format_plural($sts['queuecount'], '1 queued operation on @base.', '@count queued operations on @base.', array(
          '@base' => $client['base']));
      $ret['queue'] = array(
        '#prefix' => '<p>',
        '#markup' => $queue_status,
        '#suffix' => '</p>',
      );

      // Client cursors information
      $cursors = db_query('SELECT rt.token, rt.entity_type, rt.bundle, rt.rcid, rt.created, rt.offset, rt.ids '
        . 'FROM {res_token} rt'
        . '  INNER JOIN {res_client} rc ON rt.rcid = rc.rcid '
        . 'WHERE rc.base = :base', array(':base' => $client['base']))->fetchAll();
      if (empty($cursors)) {
        $ret['cursors'] = array(
          '#markup' => t('@base has no active cursors on <a href="!server">this server</a>.', array(
            '@base' => $client['base'],
            '!server' => url('admin/content/migrate_remote'),
        )));
      }
      else {
        $header = array(
          t('Entity/bundle'),
          t('Progress'),
          t('Created'),
          t('Operations'),
        );
        $rows = array();
        foreach ($cursors as $cursor) {
          $row = array();
          $row[] = t('@entity/@bundle', array('@entity' => $cursor->entity_type, '@bundle' => $cursor->bundle ? $cursor->bundle : '@all'));
          $ids = unserialize($cursor->ids);
          $row[] = (array_search($cursor->offset, $ids) === FALSE)
            ? t('Unknown')
            : t('@offset / @count', array('@offset' => $cursor->offset, '@count' => count($ids)));
          $row[] = format_date($cursor->created, 'custom', 'Y-m-d H:i:s');
          $row[] = l(t('Close'), 'admin/content/migrate_remote/close/' . $client['rcid'] . '/' . $cursor->token);
          $rows[] = $row;
        }
        $ret['cursors'] = array(
          '#theme' => 'table',
          '#header' => $header,
          '#rows' => $rows,
          '#caption' => t('Current cursors for @base', array('@base' => $client['base'])),
        );
      }
    }
  }

  return $ret;
}

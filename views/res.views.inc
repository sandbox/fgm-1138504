<?php

/**
 * Implements hook_views_data()
 */
function res_views_data() {
  $data = array();
  $data['res_client'] = array(
    'table' => array(
      'group' => t('RES'),
      'base'  => array(
        'field' => 'rcid',
        'title' => t('Migrate Remote client'),
        'help'  => t('A client id.'),
        'defaults' => array(
          'field'    => 'rcid',
        ),
      ),
      'join' => array(
        'res_token' => array(
          'left_table' => 'res_token', // Because this is a direct link it could be left out.
          'left_field' => 'rcid',
          'field' => 'rcid',
        ),
      )
    ), // table

    // ----------------------------------------------------------------
    // res_client table -- fields

    // rcid
    'rcid' => array(
      'title' => t('rcid'),
      'help' => t('The remote client ID.'), // The help that appears on the UI,
      // Information for displaying the nid
      'field' => array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      // Information for accepting a nid as an argument
      'argument' => array(
        'handler' => 'views_handler_argument_numeric',
        // 'name field' => 'title', // the field to display in the summary.
        'numeric' => TRUE,
        // 'validate type' => 'nid',
      ),
      // Information for accepting a nid as a filter
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      // Information for sorting on a nid.
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ), // rcid

    'base' => array(
      'title' => t('Base URL'),
      'label' => t('URL'),
      'help' => t('The base URL for the client site'),
      'field' => array(
        'handler' => 'views_handler_field_url',
        // 'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter',
        // 'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument',
      ),
    ), // base

    'endpoint' => array(
      'title' => t('Endpoint URL'),
      'label' => t('Endpoint'),
      'help' => t('The endpoint URL for the client site'),
      'field' => array(
        'handler' => 'views_handler_field_url',
        // 'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter',
        // 'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument',
      ),
    ), // base

    'status' => array(
      'title' => t('Enabled'),
      'help' => t('Is the client site enabled ?'),
      'field' => array(
        'handler' => 'views_handler_field_boolean',
        // 'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_boolean_operator',
        // 'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort_numeric',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_numeric',
      ),
    ), // base
  );


  $data['res_client_rpc'] = array(
    'table' => array(
      'group' => t('RES'),
      'base' => array(
        'field' => 'rcid',
        'title' => t('RES Client: Migrations'),
        'help' => t('Migrations defined on a client'),
        'query class' => 'res_query',
      ),
    ),
    // rcid
    'rcid' => array(
      'title' => t('rcid'),
      'help' => t('The remote client ID.'), // The help that appears on the UI,
      // Information for displaying the nid
      'field' => array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      // Information for accepting a nid as an argument
      'argument' => array(
        'handler' => 'views_handler_argument_numeric',
        // 'name field' => 'title', // the field to display in the summary.
        'numeric' => TRUE,
        // 'validate type' => 'nid',
      ),
      // Information for accepting a nid as a filter
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      // Information for sorting on a nid.
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ), // rcid
  );

  $data['res_token'] = array(
    'table' => array(
      'group' => t('RES'),
      'base'  => array(
        'field' => 'token',
        'title' => t('Migrate Remote cursor token'),
        'help'  => t('A cursor for a client.'),
        'defaults' => array(
          'field'    => 'token',
        ),
      ),
    ),  // table

    // ----------------------------------------------------------------
    // res_client table -- fields
    'token' => array(
      'title' => t('Token'),
      'help' => t('The remote client token (opaque).'), // The help that appears on the UI,
      // Information for displaying the token
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      // Information for accepting a token as a filter
      'filter' => array(
        'handler' => 'views_handler_filter',
      ),
      // Information for sorting on a token.
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ), // token
    'entity_type' => array(
      'title' => t('Entity'),
      'help' => t('The type of the entity being migrated'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter',
      ),
      // Information for sorting on a token.
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ),
    'bundle' => array(
      'title' => t('Bundle'),
      'help' => t('The bundle of the entity being migrated'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter',
      ),
      // Information for sorting on a token.
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ),
    'rcid' => array(
      'title' => t('RCID'),
      'help' => t('The remote client ID.'), // The help that appears on the UI,
      // Information for displaying the nid
      'field' => array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      // Information for accepting a nid as an argument
      'argument' => array(
        'handler' => 'views_handler_argument_numeric',
        // 'name field' => 'title', // the field to display in the summary.
        'numeric' => TRUE,
        // 'validate type' => 'nid',
      ),
      // Information for accepting a nid as a filter
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      // Information for sorting on a nid.
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ), // rcid
    'created' => array(
      'title' => t('Creation date'), // The item it appears as on the UI,
      'help' => t('The date the cursor was opened.'), // The help that appears on the UI,
      'field' => array(
        'handler' => 'views_handler_field_date',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort_date',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_date',
      ),
    ),
    'offset' => array(
      'title' => t('Offset'),
      'help' => t('The cursor offset.'), // The help that appears on the UI,
      // Information for displaying the offset
      'field' => array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      // Information for sorting on offset.
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ), // offset
    'idcount' => array(
      'title' => t('Entity count'),
      'help' => t('The number of entity ids the cursor will iterate over.'), // The help that appears on the UI,
      'real field' => 'ids',
      // Information for displaying the offset
      'field' => array(
        'handler' => 'res_views_handler_field_token_idlist',
        'click sortable' => TRUE,
      ),
      // Information for sorting on offset.
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ), // offset
    'kill' => array(
      'title' => t('Close cursor'),
      'help' => t('This requests a cursor close'),
      'real field' => 'token',
      'field' => array(
        'handler' => 'res_views_handler_field_token_close',
        'additional fields' => array('rcid'),
      ),
    ),
  );

  // dsm($data, __FUNCTION__);
  return $data;
}


/**
 * Implements hook_views_plugins().
 */
function res_views_plugins() {
  $plugins = array(
    'module' => 'res',
    'query' => array(
      'res_query' => array(
        'title' => t('migrateRemote Query'),
        'help' => t('Query will be run using the migrateRemote API.'),
        'handler' => 'res_views_plugin_query',
        'path' => drupal_get_path('module', 'res') .'/views',
      ),
    ),
  );

  // dsm($plugins, __FUNCTION__);
  return $plugins;
}

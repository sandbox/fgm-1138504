<?php
$view = new view;
$view->name = 'res_token';
$view->description = 'A list of open client migration cursors';
$view->tag = 'Migrate Remote';
$view->base_table = 'res_token';
$view->human_name = 'Migrate Remote cursors';
$view->core = 7;
$view->api_version = '3.0-alpha1';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Migrate Remote client cursors';
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'migration information';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['style_plugin'] = 'table';
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['empty'] = FALSE;
/* Field: RES: Base URL */
$handler->display->display_options['fields']['base']['id'] = 'base';
$handler->display->display_options['fields']['base']['table'] = 'res_client';
$handler->display->display_options['fields']['base']['field'] = 'base';
$handler->display->display_options['fields']['base']['label'] = 'RES Client';
$handler->display->display_options['fields']['base']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['base']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['base']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['base']['alter']['external'] = 0;
$handler->display->display_options['fields']['base']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['base']['alter']['trim'] = 0;
$handler->display->display_options['fields']['base']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['base']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['base']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['base']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['base']['alter']['html'] = 0;
$handler->display->display_options['fields']['base']['element_label_colon'] = 1;
$handler->display->display_options['fields']['base']['element_default_classes'] = 1;
$handler->display->display_options['fields']['base']['hide_empty'] = 0;
$handler->display->display_options['fields']['base']['empty_zero'] = 0;
$handler->display->display_options['fields']['base']['display_as_link'] = 1;
/* Field: RES: Entity */
$handler->display->display_options['fields']['entity_type']['id'] = 'entity_type';
$handler->display->display_options['fields']['entity_type']['table'] = 'res_token';
$handler->display->display_options['fields']['entity_type']['field'] = 'entity_type';
$handler->display->display_options['fields']['entity_type']['exclude'] = TRUE;
$handler->display->display_options['fields']['entity_type']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['entity_type']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['entity_type']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['entity_type']['alter']['external'] = 0;
$handler->display->display_options['fields']['entity_type']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['entity_type']['alter']['trim'] = 0;
$handler->display->display_options['fields']['entity_type']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['entity_type']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['entity_type']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['entity_type']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['entity_type']['alter']['html'] = 0;
$handler->display->display_options['fields']['entity_type']['element_label_colon'] = 1;
$handler->display->display_options['fields']['entity_type']['element_default_classes'] = 1;
$handler->display->display_options['fields']['entity_type']['hide_empty'] = 0;
$handler->display->display_options['fields']['entity_type']['empty_zero'] = 0;
/* Field: RES: Bundle */
$handler->display->display_options['fields']['bundle']['id'] = 'bundle';
$handler->display->display_options['fields']['bundle']['table'] = 'res_token';
$handler->display->display_options['fields']['bundle']['field'] = 'bundle';
$handler->display->display_options['fields']['bundle']['exclude'] = TRUE;
$handler->display->display_options['fields']['bundle']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['bundle']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['bundle']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['bundle']['alter']['external'] = 0;
$handler->display->display_options['fields']['bundle']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['bundle']['alter']['trim'] = 0;
$handler->display->display_options['fields']['bundle']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['bundle']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['bundle']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['bundle']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['bundle']['alter']['html'] = 0;
$handler->display->display_options['fields']['bundle']['element_label_colon'] = 1;
$handler->display->display_options['fields']['bundle']['element_default_classes'] = 1;
$handler->display->display_options['fields']['bundle']['hide_empty'] = 0;
$handler->display->display_options['fields']['bundle']['empty_zero'] = 0;
/* Field: Global: Custom text */
$handler->display->display_options['fields']['nothing']['id'] = 'nothing';
$handler->display->display_options['fields']['nothing']['table'] = 'views';
$handler->display->display_options['fields']['nothing']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing']['label'] = 'Entity/bundle';
$handler->display->display_options['fields']['nothing']['alter']['text'] = '[entity_type]/[bundle]';
$handler->display->display_options['fields']['nothing']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['external'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['trim'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['nothing']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['nothing']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['html'] = 0;
$handler->display->display_options['fields']['nothing']['element_label_colon'] = 1;
$handler->display->display_options['fields']['nothing']['element_default_classes'] = 1;
$handler->display->display_options['fields']['nothing']['hide_empty'] = 0;
$handler->display->display_options['fields']['nothing']['empty_zero'] = 0;
/* Field: RES: Creation date */
$handler->display->display_options['fields']['created']['id'] = 'created';
$handler->display->display_options['fields']['created']['table'] = 'res_token';
$handler->display->display_options['fields']['created']['field'] = 'created';
$handler->display->display_options['fields']['created']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['created']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['created']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['created']['alter']['external'] = 0;
$handler->display->display_options['fields']['created']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['created']['alter']['trim'] = 0;
$handler->display->display_options['fields']['created']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['created']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['created']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['created']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['created']['alter']['html'] = 0;
$handler->display->display_options['fields']['created']['element_label_colon'] = 1;
$handler->display->display_options['fields']['created']['element_default_classes'] = 1;
$handler->display->display_options['fields']['created']['hide_empty'] = 0;
$handler->display->display_options['fields']['created']['empty_zero'] = 0;
$handler->display->display_options['fields']['created']['date_format'] = 'short';
/* Field: RES: Offset */
$handler->display->display_options['fields']['offset']['id'] = 'offset';
$handler->display->display_options['fields']['offset']['table'] = 'res_token';
$handler->display->display_options['fields']['offset']['field'] = 'offset';
$handler->display->display_options['fields']['offset']['exclude'] = TRUE;
$handler->display->display_options['fields']['offset']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['offset']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['offset']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['offset']['alter']['external'] = 0;
$handler->display->display_options['fields']['offset']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['offset']['alter']['trim'] = 0;
$handler->display->display_options['fields']['offset']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['offset']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['offset']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['offset']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['offset']['alter']['html'] = 0;
$handler->display->display_options['fields']['offset']['element_label_colon'] = 1;
$handler->display->display_options['fields']['offset']['element_default_classes'] = 1;
$handler->display->display_options['fields']['offset']['hide_empty'] = 0;
$handler->display->display_options['fields']['offset']['empty_zero'] = 0;
$handler->display->display_options['fields']['offset']['separator'] = '';
$handler->display->display_options['fields']['offset']['format_plural'] = 0;
/* Field: RES: Entity count */
$handler->display->display_options['fields']['idcount']['id'] = 'idcount';
$handler->display->display_options['fields']['idcount']['table'] = 'res_token';
$handler->display->display_options['fields']['idcount']['field'] = 'idcount';
$handler->display->display_options['fields']['idcount']['exclude'] = TRUE;
$handler->display->display_options['fields']['idcount']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['idcount']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['idcount']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['idcount']['alter']['external'] = 0;
$handler->display->display_options['fields']['idcount']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['idcount']['alter']['trim'] = 0;
$handler->display->display_options['fields']['idcount']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['idcount']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['idcount']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['idcount']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['idcount']['alter']['html'] = 0;
$handler->display->display_options['fields']['idcount']['element_label_colon'] = 1;
$handler->display->display_options['fields']['idcount']['element_default_classes'] = 1;
$handler->display->display_options['fields']['idcount']['hide_empty'] = 0;
$handler->display->display_options['fields']['idcount']['empty_zero'] = 0;
/* Field: Global: Custom text */
$handler->display->display_options['fields']['nothing_1']['id'] = 'nothing_1';
$handler->display->display_options['fields']['nothing_1']['table'] = 'views';
$handler->display->display_options['fields']['nothing_1']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing_1']['label'] = 'Progress';
$handler->display->display_options['fields']['nothing_1']['alter']['text'] = '[offset]/[idcount]';
$handler->display->display_options['fields']['nothing_1']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['nothing_1']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['nothing_1']['alter']['external'] = 0;
$handler->display->display_options['fields']['nothing_1']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['nothing_1']['alter']['trim'] = 0;
$handler->display->display_options['fields']['nothing_1']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['nothing_1']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['nothing_1']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['nothing_1']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['nothing_1']['alter']['html'] = 0;
$handler->display->display_options['fields']['nothing_1']['element_label_colon'] = 1;
$handler->display->display_options['fields']['nothing_1']['element_default_classes'] = 1;
$handler->display->display_options['fields']['nothing_1']['hide_empty'] = 0;
$handler->display->display_options['fields']['nothing_1']['empty_zero'] = 0;
/* Field: RES: RCID */
$handler->display->display_options['fields']['rcid']['id'] = 'rcid';
$handler->display->display_options['fields']['rcid']['table'] = 'res_token';
$handler->display->display_options['fields']['rcid']['field'] = 'rcid';
$handler->display->display_options['fields']['rcid']['label'] = 'Client ID';
$handler->display->display_options['fields']['rcid']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['rcid']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['rcid']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['rcid']['alter']['external'] = 0;
$handler->display->display_options['fields']['rcid']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['rcid']['alter']['trim'] = 0;
$handler->display->display_options['fields']['rcid']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['rcid']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['rcid']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['rcid']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['rcid']['alter']['html'] = 0;
$handler->display->display_options['fields']['rcid']['element_label_colon'] = 1;
$handler->display->display_options['fields']['rcid']['element_default_classes'] = 1;
$handler->display->display_options['fields']['rcid']['hide_empty'] = 0;
$handler->display->display_options['fields']['rcid']['empty_zero'] = 0;
$handler->display->display_options['fields']['rcid']['separator'] = '';
$handler->display->display_options['fields']['rcid']['format_plural'] = 0;
/* Contextual filter: RES: RCID */
$handler->display->display_options['arguments']['rcid']['id'] = 'rcid';
$handler->display->display_options['arguments']['rcid']['table'] = 'res_token';
$handler->display->display_options['arguments']['rcid']['field'] = 'rcid';
$handler->display->display_options['arguments']['rcid']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['rcid']['summary']['format'] = 'default_summary';

/* Display: Block */
$handler = $view->new_display('block', 'Block', 'block_res_token');
$handler->display->display_options['defaults']['empty'] = FALSE;
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['label'] = 'No cursors';
$handler->display->display_options['empty']['area']['empty'] = FALSE;
$handler->display->display_options['empty']['area']['content'] = 'There is currently no open migration cursor on this server for this client.';
$handler->display->display_options['empty']['area']['tokenize'] = 0;
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: RES: RCID */
$handler->display->display_options['arguments']['rcid']['id'] = 'rcid';
$handler->display->display_options['arguments']['rcid']['table'] = 'res_token';
$handler->display->display_options['arguments']['rcid']['field'] = 'rcid';
$handler->display->display_options['arguments']['rcid']['default_action'] = 'default';
$handler->display->display_options['arguments']['rcid']['default_argument_type'] = 'php';
$handler->display->display_options['arguments']['rcid']['default_argument_options']['code'] = 'return is_numeric(arg(3)) ? arg(3) : FALSE;';
$handler->display->display_options['arguments']['rcid']['default_argument_skip_url'] = 0;
$handler->display->display_options['arguments']['rcid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['rcid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['rcid']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['rcid']['break_phrase'] = 0;
$handler->display->display_options['arguments']['rcid']['not'] = 0;
$translatables['res_token'] = array(
  t('Master'),
  t('Migrate Remote client cursors'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Items per page'),
  t('- All -'),
  t('Offset'),
  t('RES Client'),
  t('Entity'),
  t('Bundle'),
  t('Entity/bundle'),
  t('[entity_type]/[bundle]'),
  t('Creation date'),
  t('.'),
  t('Entity count'),
  t('Progress'),
  t('[offset]/[idcount]'),
  t('Client ID'),
  t('All'),
  t('Block'),
  t('No cursors'),
  t('There is currently no open migration cursor on this server for this client.'),
);

<?php
$view = new view;
$view->name = 'res_client';
$view->description = 'Migrate Remote clients defined for this server site';
$view->tag = 'Migrate Remote';
$view->base_table = 'res_client';
$view->human_name = 'Migrate Remote clients overview';
$view->core = 7;
$view->api_version = '3.0-alpha1';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Migrate Remote clients';
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'migration information';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'rcid' => 'rcid',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'rcid' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
);
$handler->display->display_options['style_options']['override'] = 1;
$handler->display->display_options['style_options']['sticky'] = 0;
$handler->display->display_options['style_options']['empty_table'] = 0;
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['empty'] = FALSE;
$handler->display->display_options['empty']['area']['content'] = 'No Migrate Remote client is currently defined. You can not use this server as a Migrate Remote source until you have defined at least one client allowed to use it.';
$handler->display->display_options['empty']['area']['tokenize'] = 0;
/* Field: RES: rcid */
$handler->display->display_options['fields']['rcid']['id'] = 'rcid';
$handler->display->display_options['fields']['rcid']['table'] = 'res_client';
$handler->display->display_options['fields']['rcid']['field'] = 'rcid';
$handler->display->display_options['fields']['rcid']['label'] = 'id';
$handler->display->display_options['fields']['rcid']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['rcid']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['rcid']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['rcid']['alter']['external'] = 0;
$handler->display->display_options['fields']['rcid']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['rcid']['alter']['trim'] = 0;
$handler->display->display_options['fields']['rcid']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['rcid']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['rcid']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['rcid']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['rcid']['alter']['html'] = 0;
$handler->display->display_options['fields']['rcid']['element_label_colon'] = 1;
$handler->display->display_options['fields']['rcid']['element_default_classes'] = 1;
$handler->display->display_options['fields']['rcid']['hide_empty'] = 0;
$handler->display->display_options['fields']['rcid']['empty_zero'] = 0;
$handler->display->display_options['fields']['rcid']['format_plural'] = 0;
/* Field: RES: Base URL */
$handler->display->display_options['fields']['base']['id'] = 'base';
$handler->display->display_options['fields']['base']['table'] = 'res_client';
$handler->display->display_options['fields']['base']['field'] = 'base';
$handler->display->display_options['fields']['base']['label'] = 'Client URL';
$handler->display->display_options['fields']['base']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['base']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['base']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['base']['alter']['external'] = 0;
$handler->display->display_options['fields']['base']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['base']['alter']['trim'] = 0;
$handler->display->display_options['fields']['base']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['base']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['base']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['base']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['base']['alter']['html'] = 0;
$handler->display->display_options['fields']['base']['element_label_colon'] = 1;
$handler->display->display_options['fields']['base']['element_default_classes'] = 1;
$handler->display->display_options['fields']['base']['hide_empty'] = 0;
$handler->display->display_options['fields']['base']['empty_zero'] = 0;
$handler->display->display_options['fields']['base']['display_as_link'] = 1;
/* Field: RES: Endpoint URL */
$handler->display->display_options['fields']['endpoint']['id'] = 'endpoint';
$handler->display->display_options['fields']['endpoint']['table'] = 'res_client';
$handler->display->display_options['fields']['endpoint']['field'] = 'endpoint';
$handler->display->display_options['fields']['endpoint']['label'] = 'Endpoint';
$handler->display->display_options['fields']['endpoint']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['endpoint']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['endpoint']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['endpoint']['alter']['external'] = 0;
$handler->display->display_options['fields']['endpoint']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['endpoint']['alter']['trim'] = 0;
$handler->display->display_options['fields']['endpoint']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['endpoint']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['endpoint']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['endpoint']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['endpoint']['alter']['html'] = 0;
$handler->display->display_options['fields']['endpoint']['element_label_colon'] = 1;
$handler->display->display_options['fields']['endpoint']['element_default_classes'] = 1;
$handler->display->display_options['fields']['endpoint']['hide_empty'] = 0;
$handler->display->display_options['fields']['endpoint']['empty_zero'] = 0;
$handler->display->display_options['fields']['endpoint']['display_as_link'] = 1;
/* Field: RES: Enabled */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'res_client';
$handler->display->display_options['fields']['status']['field'] = 'status';
$handler->display->display_options['fields']['status']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['status']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['status']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['status']['alter']['external'] = 0;
$handler->display->display_options['fields']['status']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['status']['alter']['trim'] = 0;
$handler->display->display_options['fields']['status']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['status']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['status']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['status']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['status']['alter']['html'] = 0;
$handler->display->display_options['fields']['status']['element_label_colon'] = 0;
$handler->display->display_options['fields']['status']['element_default_classes'] = 1;
$handler->display->display_options['fields']['status']['hide_empty'] = 0;
$handler->display->display_options['fields']['status']['empty_zero'] = 0;
$handler->display->display_options['fields']['status']['not'] = 0;
/* Field: RES: Base URL */
$handler->display->display_options['fields']['base_1']['id'] = 'base_1';
$handler->display->display_options['fields']['base_1']['table'] = 'res_client';
$handler->display->display_options['fields']['base_1']['field'] = 'base';
$handler->display->display_options['fields']['base_1']['label'] = 'Status';
$handler->display->display_options['fields']['base_1']['alter']['alter_text'] = 1;
$handler->display->display_options['fields']['base_1']['alter']['text'] = 'view';
$handler->display->display_options['fields']['base_1']['alter']['make_link'] = 1;
$handler->display->display_options['fields']['base_1']['alter']['path'] = 'admin/content/migrate_remote/[rcid]';
$handler->display->display_options['fields']['base_1']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['base_1']['alter']['external'] = 0;
$handler->display->display_options['fields']['base_1']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['base_1']['alter']['trim'] = 0;
$handler->display->display_options['fields']['base_1']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['base_1']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['base_1']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['base_1']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['base_1']['alter']['html'] = 0;
$handler->display->display_options['fields']['base_1']['element_label_colon'] = 1;
$handler->display->display_options['fields']['base_1']['element_default_classes'] = 1;
$handler->display->display_options['fields']['base_1']['hide_empty'] = 0;
$handler->display->display_options['fields']['base_1']['empty_zero'] = 0;
$handler->display->display_options['fields']['base_1']['display_as_link'] = 0;

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'admin/content/migrate_remote/overview';
$handler->display->display_options['menu']['type'] = 'default tab';
$handler->display->display_options['menu']['title'] = 'Clients';
$handler->display->display_options['menu']['description'] = 'Migrate Remote';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['tab_options']['type'] = 'tab';
$handler->display->display_options['tab_options']['title'] = 'Migrate Server';
$handler->display->display_options['tab_options']['weight'] = '0';
$translatables['res_client'] = array(
  t('Master'),
  t('Migrate Remote clients'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Items per page'),
  t('- All -'),
  t('Offset'),
  t('No Migrate Remote client is currently defined. You can not use this server as a Migrate Remote source until you have defined at least one client allowed to use it.'),
  t('id'),
  t('.'),
  t(','),
  t('Client URL'),
  t('Endpoint'),
  t('Enabled'),
  t('Status'),
  t('view'),
  t('admin/content/migrate_remote/[rcid]'),
  t('Page'),
);

<?php
/**
 * @file
 *
 * A simple query plugin that just know how to invoke the migrateRemote service.
 */

class res_views_plugin_query extends views_plugin_query {
  function execute(&$view) {
    parent::execute($view);
    $view->result = NULL;
    $view->total_rows = 0;
    $view->execute_time = 0;
    $view->pager['current_page'] = 0;
  }

  function validate() {
    vsm(__FUNCTION__);
    return parent::validate();
  }

  function __call($name, $args) {
    vsm(t('__call @name(@args)', array('@name' => $name, '@args' => array_shift(func_get_args()))));
  }
}

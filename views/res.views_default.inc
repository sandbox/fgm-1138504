<?php
function res_views_default_views() {
  $path = drupal_get_path('module', 'res') . '/views';
  $dir = new DirectoryIterator($path);
  $views = array();

  foreach ($dir as $info) {
    if ($info->isDir()) { // includes, but not limited to, '.' and '..'
      continue;
    }
    elseif (!preg_match('/res_.*\.view\.inc$/', $info->getFilename())) { // not a RES view
      continue;
    }
    else {
      // dsm("Requiring {$info->getPathname()}");
      require $info->getPathname();
      $views[$view->name] = $view;
      unset($view);
    }
  }

  return $views;
}
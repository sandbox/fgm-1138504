<?php

/**
 * Field handler to provide simple renderer that turns a URL into a clickable link.
 *
 * @ingroup views_field_handlers
 */
class res_views_handler_field_token_idlist extends views_handler_field {
  function render($values) {
    $value = $this->get_value($values);
    $value = unserialize($value);
    return count($value);
  }
}

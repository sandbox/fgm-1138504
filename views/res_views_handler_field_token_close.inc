<?php

/**
 * Field handler to provide a link to a cursor close action
 *
 * @ingroup views_field_handlers
 */
class res_views_handler_field_token_close extends views_handler_field {
  function render($values) {
    $rcid = $this->get_value($values, 'rcid');
    $value = $this->get_value($values);
    $ret = l(t('Close'), 'admin/content/migrate_remote/close/' . $rcid . '/' . $value);
    return $ret;
  }
}

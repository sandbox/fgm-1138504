<?php
/**
 * Migrate v2 Destination class for Migrate Remote RES.
 *
 * @copyright (c) 2011 OSInet
 *
 * @author Frédéric G. MARAND (fgm@osinet.fr)
 *
 * Licensed under the General Public License version 2 or later.
 */

class MigrateDestinationRes extends MigrateDestinationEntityAPI {

  /**
   * Keeps track of migrated entity IDs.
   *
   * @var array
   */
  public $entityIds = array();

  /**
   * Basic initialization.
   */
  public function __construct($entity_type, $bundle = NULL, array $options = array()) {
    parent::__construct($entity_type, $bundle, $options);
  }

  /**
   * Overrides MigrateDestinationEntityAPI::fields().
   *
   * No fields are mapped because entities are copied transparently.
   */
  public function fields() {
    return array();
  }

  /**
   * RES migration is transparent between identical entity implementations on
   * source and destination, so DON'T invoke parent::prepare() in order to avoid
   * any MigrateHandlerField intervention.
   *
   * @see MigrateDestinationEntity::prepare()
   */
  public function prepare($entity, stdClass $source_row) {
    $migration = Migration::currentMigration();
    $entity->migrate = array(
      'machineName' => $migration->getMachineName(),
    );

    if (method_exists($migration, 'prepare')) {
      $migration->prepare($entity, $source_row);
    }
  }

  public function import(stdClass $entity, stdClass $row) {
    $return = parent::import($entity, $row);

    if ($return) {
      $this->entityIds[] = $return[0];
    }

    return $return;
  }
}

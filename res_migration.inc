<?php

/**
 * Migrations using RES sources should extend this class instead of Migration.
 *
 * This will allow them to mark Field API fields for flattening during mapping.
 */
abstract class ResMigration extends Migration {

  /**
   * The bundle being migrated for this entity.
   *
   * @var string
   */
  protected $bundle = NULL;

  /**
   * Endpoint of the CatMS server.
   *
   * @var string
   */
  protected $server_url = NULL;

  /**
   * The type of the entity being migrated
   *
   * @var string
   */
  protected $entity_type = NULL;

  function __construct(array $arguments, $group = NULL) {
    parent::__construct($group);

    // Merge in defaults.
    $arguments += array(
      'bundle' => NULL,
      'description' => '',
    );

    $this->server_url   = variable_get('res_server_url', RES_SERVER_URL);

    $this->description = $arguments['description'];
    $this->entity_type = $arguments['entity_type'];
    $this->bundle      = $arguments['bundle'];

    $source_options = isset($arguments['source_options'])
      ? $arguments['source_options']
      : array();

    $this->source = new MigrateSourceRes($this->server_url, $this->entity_type, $this->bundle, $source_options);
    $this->destination = new MigrateDestinationRes($this->entity_type, $this->bundle, $options = array());
    $this->map = new MigrateSQLMap(
      $this->machineName, // set by MigrationBase::__construct()
      $this->source->_getDefaultKeys(),
      MigrateDestinationEntityAPI::getKeySchema($this->entity_type)
    );

    // Tell migrate to use and maintain the highwater field. This will only work
    // if the export source supports it.
    $this->highwaterField = array('name' => '_res_sequence');
  }

  /**
   * Override of Migration::applyMappings().
   */
  protected function applyMappings() {
    $entity = clone $this->sourceValues;

    // Remove deafault source keys from the mapping as this causes problems when
    // updating migrated entities.
    foreach ($this->source->_getDefaultKeys() as $key => $label) {
      unset($entity->{$key});
    }

    $this->destinationValues = (object) array();
    $this->processEntity($this->entity_type, $entity);
    $this->destinationValues = $entity;
  }

  /**
   * (Internal) Process the entity for export.
   */
  protected function processEntity($entity_type, $entity) {
    $entity_info = entity_get_info($entity_type);
    list($entity_id, $revision_id, $bundle) = entity_extract_ids($entity_type, $entity);

    if (!empty($entity_info['base table'])) {
      $schema = drupal_get_schema($entity_info['base table']);
      if (!empty($schema['foreign keys'])) {
        $this->applySourceMigrationFromForeignKeys($schema, $entity);
      }
    }

    // Do the same for fields.
    foreach (field_info_instances($entity_type, $bundle) as $instance) {
      if (!isset($entity->{$instance['field_name']})) {
        continue;
      }

      $field_info = field_info_field($instance['field_name']);
      if (!empty($field_info['foreign keys'])) {
        foreach ($entity->{$instance['field_name']} as $langcode => $items) {
          foreach ($items as $delta => $item) {
            $item = (object) $item;
            $this->applySourceMigrationFromForeignKeys($field_info, $item);
            $entity->{$instance['field_name']}[$langcode][$delta] = (array) $item;
          }
        }
      }
    }
  }

  /**
   * Helper function: try to load an entity type from a base table name.
   */
  protected function entityGetTypeByTable($table) {
    foreach (entity_get_info() as $entity_type => $entity_info) {
      if (isset($entity_info['base table']) && $entity_info['base table'] == $table) {
        return $entity_type;
      }
    }

    return FALSE;
  }

  /**
   * Helper function: apply mapping of foreign keys from other source migrations.
   */
  protected function applySourceMigrationFromForeignKeys($schema, &$data) {
    foreach ($schema['foreign keys'] as $foreign_key) {
      $table = $foreign_key['table'];
      $foreign_entity_type = $this->entityGetTypeByTable($table);
      if (!$foreign_entity_type || count($foreign_key['columns']) != 1) {
        // The foreign key does not point to an entity.
        continue;
      }

      $source_column = key($foreign_key['columns']);
      if (!isset($data->{$source_column})) {
        continue;
      }

      // Load the ID from the migration.
      $source_migration = $this->getMigration($foreign_entity_type);
      if (!$source_migration) {
        // No source migration, ignore the mapping.
        // Implementations wishing this condition to be fatal can throw an
        // exception in getMigration().
        continue;
      }

      // Create an array of source IDs. If there are more than one source keys,
      // all values will be defaulted to the one from the first source key, as
      // we have no way of knowing the other values (revision_id for example).
      $destination_key_schema = MigrateDestinationEntityAPI::getKeySchema($foreign_entity_type);
      $source_ids = array();
      foreach ($destination_key_schema as $column => $schema) {
        $source_ids[] = $data->{$source_column};
      }

      $destination_ids = $source_migration->getMap()->lookupDestinationID($source_ids);
      if (!$destination_ids) {
        $destination_ids = $source_migration->createStubWrapper($source_ids, $this);
      }
      if (!$destination_ids) {
        throw new MigrateException(t('Unable to resolve foreign key for @entity_type:@source_id', array('@entity_type' => $foreign_entity_type, '@source_id' => $data->{$source_column})));
      }

      $data->{$source_column} = reset($destination_ids);
    }
  }

  /**
   * Get the migration associated to an entity type.
   */
  abstract protected function getMigration($entity_type);
}

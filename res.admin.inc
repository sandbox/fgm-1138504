<?php
function res_cursor_close_confirm_form($form, $form_state, $client, $token) {
  $path = 'admin/content/migrate_remote/' . $client['rcid'];
  $form['path']  = array('#type' => 'value', '#value' => $path);
  $form['token'] = array('#type' => 'value', '#value' => $token);
  $form['base']  = array('#type' => 'value', '#value' => $client['base']);

  $ret = confirm_form($form,
    t('Do you confirm closing cursor %cursor for client %base', array(
      '%cursor' => $token,
      '%base' => $client['base'])),
    $path
  );
  return $ret;
}

function res_cursor_close_confirm_form_submit($form, &$form_state) {
  $form_state['redirect'] = $form_state['values']['path'];
  drupal_set_message(t('Cursor for %base deleted', array('%base' => $form_state['values']['base'])));

  db_delete('res_token')
    ->condition('token', $form_state['values']['token'])
    ->execute();
}

/**
 * Form builder for admin/content/migrate_remote/settings
 *
 *
 * @param array $form
 * @param array $form_state
 *
 * @return array
 */
function res_settings_form($form, $form_state) {
  drupal_set_title(t('Migrate Server: settings'));
  $bc = _res_get_base_breadcrumb();
  $bc[] = l(t('Migrate Server'), 'admin/content/migrate_remote');
  drupal_set_breadcrumb($bc);

  $form['res_max_token_age'] = array(
    '#type' => 'textfield',
    '#title' => t('Token max age'),
    '#description' => t('Default maximum lifetime for migration tokens. 1 day = 86400, 1 week = 604800, 30 days = 2592000'),
    '#default_value' => variable_get('res_max_token_age', 86400),
    '#maxlength' => 10,
    '#size' => 10,
  );

  $form['res_server_url'] = array(
    '#title' => t('RES Server URL'),
    '#description' => t('The absolute URL of the RES server XML-RPC endpoint.'),
    '#type'        => 'textfield',
    '#default_value' => variable_get('res_server_url', RES_SERVER_URL),
  );

  return system_settings_form($form);
}

<?php
/**
 * @file
 * Hooks provided by the Migrate Remote RES API.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Warn of an impending building of a record set by RES.
 *
 * This can be used by modules wishing to alter the entity information just for
 * RES' sake, possibly to alter the results intended for a specific client.
 *
 * @see res_get_row_count()
 * @see hook_res_post_count()
 *
 * @param array $params
 * - 'entity_type'
 * - 'bundle'
 * - 'base' the base URL of the RES client site
 */
function example_res_pre_record_set($params) {
  $entity_name = $bundle_name = $base = NULL;
  extract($params, EXTR_IF_EXISTS);

  // Do something, for instance altering the schema for $entity_name
}

/**
 * Warn of a performed building a record set by RES.
 *
 * This can be used by modules wishing to alter the entity information just for
 * RES' sake, possibly to restore some state information altered by an
 * implementation of hook_res_pre_count()
 *
 * @see res_get_rows()
 * @see hook_res_pre_count()
 *
 * @param array $ids
 *   The built list of entity IDs.
 * @param array $parameters
 *   - entity_type
 *   - bundle
 *   - base
 */
function example_res_record_set_alter($ids, $parameters) {
  // Do something, for instance restoring an altered schema for $entity_type
}

/**
 * Warn of an impending entity load by RES.
 *
 * This can be used by modules wishing to alter the entity information just for
 * RES' sake, possibly to alter the results intended for a specific client.
 *
 * @see res_get_rows()
 * @see hook_res_post_load()
 *
 * @param array $params
 * - 'entity_type' the name of the entity from which RES will be loading
 * - 'base' the base URL of the RES client site
 * - 'ids' a list of the entity ids RES will be loading
 *
 * @return boolean
 *   FALSE if RES should NOT go ahead with load.
 */
function example_res_pre_load_alter($params) {
  $entity_name = $base = $ids = NULL;
  extract($params, EXTR_IF_EXISTS);

  // Do something, for instance altering the schema for $entity_name
}

/**
 * Warn of a performed entity load by RES.
 *
 * This can be used by modules wishing to alter the entity information just for
 * RES' sake, possibly to restore some state information altered by an
 * implementation of hook_res_pre_load()
 *
 * @see res_get_rows()
 * @see hook_res_pre_load()
 *
 * @param array $params
 * - 'entity_type' the name of the entity from which RES will be loading
 * - 'bundle' the name of the bundle from which RES will be loading, or NULL
 * - 'token' the current remote cursor (opaque string)
 * - 'base' the base URL of the RES client site
 * - 'rows' array of the entities to be returned
 *
 * @return void
 */
function example_res_post_load_alter($params) {
  // Do something, for instance restoring an altered schema for $entity_name
  // or adding a non-entity property like the parents on a taxonomy term
}

/**
 * Warn of an impending entity schema extraction by RES.
 *
 * This can be used by modules wishing to alter the entity information just for
 * RES' sake, possibly to alter the results intended for a specific client.
 *
 * @see res_get_row_count()
 * @see hook_res_post_schema()
 *
 * @param array $params
 *   - 'entity_type'
 *   - 'bundle'
 *   - 'base' the base URL of the RES client site
 * @param array $entity_info
 *   An array as returned by entity_get_info().
 * @param array $property_info
 *   An array as returned by entity_get_all_property_info().
 *
 * @return boolean
 *   FALSE if RES should NOT go ahead with schema extraction.
 */
function example_res_pre_schema_alter($params, $entity_info, $property_info) {
  $entity_name = $bundle_name = $base = NULL;
  extract($params, EXTR_IF_EXISTS);

  // Do something, for instance altering the schema for $entity_name
}

/**
 * Warn of a performed entity schema extraction by RES.
 *
 * This can be used by modules wishing to alter the entity information just for
 * RES' sake, possibly to restore some state information altered by an
 * implementation of hook_res_pre_schema()
 *
 * @see res_get_rows()
 * @see hook_res_pre_schema()
 *
 * @param array $params
 * - 'entity_type'
 * - 'bundle'
 * - 'base' the base URL of the RES client site
 * - 'count' the result of the count
 *
 * @return boolean
 *   FALSE if RES should NOT return the count result
 */
function example_res_post_schema_alter($params) {
  $entity_name = $bundle_name = $base = $count = NULL;
  extract($params, EXTR_IF_EXISTS);

  // Do something, for instance restoring an altered schema for $entity_name
}

/**
 * @} End of "addtogroup hooks".
 */

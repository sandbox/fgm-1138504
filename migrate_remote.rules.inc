<?php

/**
 * @file
 * Rules integration for Cartier Pricing strategy.
 *
 * @addtogroup rules
 * @{
 */


/**
 * Implements hook_rules_action_info().
 */
function migrate_remote_rules_action_info() {
  $actions['migrate_remote_ping'] = array(
    'label' => t('Ping client migrate sites'),
    'parameter' => array(),
    'group' => t('Migrate Remote'),
    'callbacks' => array(
      'execute' => 'migrate_remote_ping',
    ),
  );

  return $actions;
}

/**
 * @}
 */

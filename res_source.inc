<?php
/**
 * Migrate v2 Source class for Migrate Remote RES.
 *
 * @copyright (c) 2011 OSInet
 *
 * @author Frédéric G. MARAND (fgm@osinet.fr)
 *
 * Licensed under the General Public License version 2 or later.
 */

/**
 * Notes:
 *
 * Possible Problem: the number of source rows changes during a migration. For
 *   higher reliability, we might want to lock a set of the data (replicate ?)
 *   until the migration set is releases. Costly in code and server resources,
 *   so not done on this version: it just stores the list of ids at each rewind,
 *   and iterates on that list until the token is released or expired.
 *
 * Naming conventions:
 * - methods named getFoo return foo, and may store it in the instance
 * - methods named fetchFoo obtain foo and store it in the instance
 * - methods name _foo are RES-specific, and not in the Migrate v2 space
 */

/**
 * Migrate source for migrate v2.
 */
class MigrateSourceRes extends MigrateSource {

  public $res_url      = NULL;    // URL of export endpoint
  public $entity_type  = NULL;    // name of imported entity
  public $bundle       = NULL;    // name of imported bundle (or NULL)
  public $fields       = NULL;    // array of [field, field_name]
  public $count        = NULL;    // number of source rows. NULL means unknown, but 0 means no row.
  public $entityInfo   = NULL;    // the entity info

  /**
   * The number of rows to fetch in a next() call. Default set in constructor.
   *
   * @var int $nextLimit
   */
  public $nextLimit        = NULL;
  /**
   * The timeout when performing a next() call. Default set in constructor.
   *
   * @var int $nextTimeout
   */
  public $nextTimeout  = NULL;

  /**
   * Has EOF been reached ?
   *
   * @var boolean
   */
  public $eof         = FALSE;

  /**
   * A local row buffer to avoid performing one RPC for each request.
   *
   * FIXME Not used correctly yet.
   *
   * @var array
   */
  public $rows        = NULL;

  /**
   * The remote schema
   *
   * @var array
   */
  public $schema      = NULL;

  /**
   * The migration token.
   *
   * Multiple migrations can be active simultaneously between the two same
   * hosts, so we need to differentiate them. This token ensures differentiation.
   *
   * @var string
   */
  public $token       = NULL;

  /**
   * Obtain the list of RES-exported data (initially entites+bundles).
   *
   * Store the export information only if the requested set of data is available.
   *
   * @param string $res_url
   * @param string $entity_type
   * @param string $bundle
   *
   * @return void
   */
  public function __construct($res_url, $entity_type, $bundle, array $options = array()) {
    1||watchdog('res', '@fun(@args)', array('@fun' => __CLASS__ . '::' . __FUNCTION__, '@args' => implode(', ', func_get_args())), WATCHDOG_DEBUG);
    parent::__construct();
    $this->res_url = $res_url;
    $this->entity_type = $entity_type;
    $this->bundle = $bundle;

    // Use explicit settings if they exist, or default ones if they are not already set.
    $default_options = array(
      'nextLimit' => 100,
      'nextTimeout' => 120,
    );
    foreach ($default_options as $name => $default) {
      if (isset($options[$name])) {
        $this->{$name} = $options[$name];
      }
      elseif (!isset($this->{$name})) {
        $this->{$name} = $default_options[$name];
      }
    }
    $this->entityInfo = entity_get_info($entity_type);
  }

  /**
   * Allow the server to release the token.
   *
   * Watchdog() calls are disabled because on some setups they cause this error:
   * PHP Fatal error:  Exception thrown without a stack frame in Unknown on line 0
   */
  public function __destruct() {
    $caller = _res_get_caller(1);
    if (!empty($this->token)) {
      $result = xmlrpc($this->res_url, array(
        'res.closeExport' => array($this->token),
      ));

      if ($result === FALSE) {
        1||watchdog('res', '@method(@args) error: @message', $caller + array('@message' => xmlrpc_error_msg()), WATCHDOG_ERROR);
      }
      else {
        1||watchdog('res', "@method(token = @token): @result\n", $caller + array('@token' => var_export($this->token, TRUE), '@result' => print_r($result, TRUE)), WATCHDOG_DEBUG);
      }
    }
    else {
      1||watchdog('res', '@method() without a current token. No RPC.', $caller, WATCHDOG_NOTICE);
    }
  }

  /**
   * Magic method to display info about the current data sourcing.
   */
  public function __toString() {
    1||watchdog('res', '@fun(@args)', array('@fun' => __CLASS__ . '::' . __FUNCTION__, '@args' => implode(', ', func_get_args())), WATCHDOG_DEBUG);
    $ret = t('RES sourcing @name from !url', array(
      '@name' => $this->entity_type . '/' . ($this->bundle ? $this->bundle : '@all'),
      '!url' => $this->res_url,
    ));
    return $ret;
  }

  /**
   * Internal function to fetch some rows.
   *
   * Modifies the current token, but not the [current]row[s] information.
   *
   * @param int $limit
   * @param array $ids
   * @param int $highwater
   */
  protected function _getExportRows($ids = array(), $highwater) {
    $caller = _res_get_caller(2);
    1||watchdog('res', '@method(@args)', $caller, WATCHDOG_DEBUG);

    if (!empty($this->token) && !empty($this->previousLimit)) {
      // If we previously fetched rows, advance the cursor forward.
      $result = xmlrpc($this->res_url, array(
        'res.nextExportRows' => array($this->token, $this->previousLimit),
      ), array('timeout' => $this->nextTimeout));
      if ($result === FALSE) {
        watchdog('res', 'res.nextExportRows : @method(@args) error: @message', $caller + array('@message' => xmlrpc_error_msg()), WATCHDOG_ERROR);
      }
      unset($this->previousLimit);
    }

    // No parent::next();
    $result = xmlrpc($this->res_url, array(
      'res.getExportRows' => array($this->entity_type, $this->bundle, $this->nextLimit, $GLOBALS['base_url'], $this->token, $ids, $highwater)
    ), array('timeout' => $this->nextTimeout));

    if ($result === FALSE) {
      watchdog('res', 'res.getExportRows : @method(@args) error: @message', $caller + array('@message' => xmlrpc_error_msg()), WATCHDOG_ERROR);
    }
    else {
      1||watchdog('res', "@method(@args): @result\n", $caller + array('@result' => print_r($result, TRUE)), WATCHDOG_DEBUG);
      $this->eof = $result['eof'];
      $result['rows'] = $this->eof
        ? array()
        : unserialize(base64_decode($result['rows']));

      $this->previousLimit = count($result['rows']);
      if (empty($this->token)) {
        $this->token = $result['token'];
      }
      elseif ($this->token != $result['token']) {
        watchdog('res', '@method(@args): inconsistent token retrieved: expected %current got %new', $caller + array(
          '%current' => $this->token,
          '%new'     => $result['token'],
        ), WATCHDOG_ERROR);
        $result = FALSE;
      }
    }
    return $result;
  }

  /**
   * Draw key information out of thin air.
   *
   * TODO: this relies on assumptions about Field API, and should be double checked.
   */
  public function _getDefaultKeys() {
    $core_entity_id_schema = array(
      'type' => 'int',
      'length' => 10,
      'not null' => TRUE,
      'description' => '',
    );

    $id = $core_entity_id_schema;
    if (!empty($this->entityInfo['entity keys']['label'])) {
      $id['description'] = t('@label id', array(
        '@label' => $this->entityInfo['entity keys']['label'],
      ));
    }
    $ret = array(
      $this->entityInfo['entity keys']['id'] => $id,
    );

    if (!empty($this->entityInfo['entity keys']['revision'])) {
      $revision = $core_entity_id_schema;
      $revision['description'] = t('@label revision', array(
        '@label' => $this->entityInfo['entity keys']['label'],
      ));
      $ret[$this->entityInfo['entity keys']['revision']] = $revision;
    }

    1||watchdog('res', '@method(@args): @ret', _res_get_caller(1) + array('@ret' => var_export($ret, TRUE)), WATCHDOG_DEBUG);
    return $ret;
  }

  /**
   * Set the current key from the current row.
   */
  protected function _fetchCurrentKey() {
    $migration = Migration::currentMigration();
    $map = $migration->getMap();
    $this->currentKey = array();

    if (empty($this->currentRow)) { // can be FALSE
      $this->currentKey = array();
    }
    else {
      foreach ($map->getSourceKey() as $field_name => $field_schema) {
        $this->currentKey[$field_name] = $this->currentRow->$field_name;
      }
    }
  }

  /**
   * Derived classes must implement count(), returning a count of all available
   * source records. If the count is cached, it must be refreshed when TRUE is passed.
   */
  public function count($refresh = FALSE) {
    1||watchdog('res', '@fun(@args)', array('@fun' => __CLASS__ . '::' . __FUNCTION__, '@args' => implode(', ', func_get_args())), WATCHDOG_DEBUG);
    if ($refresh || !isset($this->count)) {
      $result = xmlrpc($this->res_url, array('res.getExportRowCount' => array($this->entity_type, $this->bundle, $GLOBALS['base_url'])));
      if ($result === FALSE) {
        $message = xmlrpc_error_msg();
        drupal_set_message(t('Failed obtaining the row count: @error', array(
          '@error' => $message)));
        watchdog('res', 'Failed obtaining the row count: @error', array(
          '@error' => $message,
        ), WATCHDOG_ERROR);
      }
      else {
        $this->count = $result;
      }
    }
    return $this->count;
  }

  /**
   * Array of available source fields for the current set/subset.
   *
   * @return array
   *   Keys: machine names of the fields (to be passed to addFieldMapping)
   *   Values: Human-friendly descriptions of the fields.
   */
  public function fields() {
    return array();
  }

  /**
   * (non-PHPdoc)
   * @see sites/all/modules/contrib/migrate/includes/MigrateSource::current()
   */
  public function current() {
    $ret = parent::current();
    1||watchdog('res', '@method(@args): @current', _res_get_caller(1) + array('@current' => var_export($ret, TRUE)), WATCHDOG_DEBUG);
    return $ret;
  }

  /**
   * (non-PHPdoc)
   * @see sites/all/modules/contrib/migrate/includes/MigrateSource::key()
   */
  public function key() {
    1||watchdog('res', '@fun(@args)', array('@fun' => __CLASS__ . '::' . __FUNCTION__, '@args' => implode(', ', func_get_args())), WATCHDOG_DEBUG);
    $ret = parent::key();
    return $ret;
  }

  /**
   * (non-PHPdoc)
   * @see Iterator::next()
   */
  public function next() {
    1||watchdog('res', '@fun(@args)', array('@fun' => __CLASS__ . '::' . __FUNCTION__, '@args' => implode(', ', func_get_args())), WATCHDOG_DEBUG);
    // No parent::next();

    // Honor the "limit" Migration option
    $migration = Migration::currentMigration();
    $item_limit = $migration->getItemLimit();
    if (empty($item_limit) || $this->numProcessed < $item_limit) {
      migrate_instrument_start(__CLASS__ . '::' . __FUNCTION__);

      if (empty($this->bufferedRows)) {
        // The default highwater is -1, which will get us all the rows stricly
        // greater then that.
        $highwater = -1;
        $highwaterField = $migration->getHighwaterField();
        if (isset($highwaterField['name']) && ($current_highwater = $migration->getHighwater()) !== '') {
          $highwater = $current_highwater;
        }
        
        $result = $this->_getExportRows(array(), (int) $highwater);

        if ($result !== FALSE) {
          $this->bufferedRows = $result['rows'];
        }
      }

      if (!empty($this->bufferedRows)) {
        // Get row data
        $this->currentRow = reset($this->bufferedRows);
        $row_key = key($this->bufferedRows);
        unset($this->bufferedRows[$row_key]);

        $this->_fetchCurrentKey();

        // Code until the end of this "if" lifted from migrate/plugins/sources/csv.inc
        // Process row
        $map_row = $migration->getMap()->getRowBySource($this->currentKey);
        if (!$map_row) {
          // Unmigrated row, take it
        }
        elseif ($map_row && $map_row['needs_update'] == 1) {
          // We always want to take this row if needs_update = 1
        }
        else {
          // Just take the row.
        }
        // Add map info to the row, if present
        if ($map_row) {
          foreach ($map_row as $field => $value) {
            $field = 'migrate_map_' . $field;
            $this->currentRow->$field = $value;
          }
        }

        // Add some debugging, just for the first row.
        if (empty($this->numProcessed)) {
          1||$migration->showMessage(print_r($this->currentRow, TRUE), 'debug');
        }

        // Allow the Migration to prepare this row. prepareRow() can return boolean
        // FALSE to stop processing this row. To add/modify fields on the
        // result, modify $row by reference.
        $return = TRUE;
        if (method_exists($migration, 'prepareRow')) {
          $return = $migration->prepareRow($this->currentRow);
        }

        if ($return !== FALSE) {
          $this->numProcessed++;
        }
      }
      else {
        $this->currentRow = $this->rows = NULL;
      }
    }
    migrate_instrument_stop(__CLASS__ . '::' . __FUNCTION__);
  }

  /**
   * (non-PHPdoc)
   * @see Iterator::rewind()
   */
  public function rewind() {
    // No parent::rewind();
    // $this->count();
    $this->numProcessed = -1;
    $this->next(); // Will increase numProcessed to 0
    1||watchdog('res', '@method(@args): @result', _res_get_caller(1) + array('@result' => $result), WATCHDOG_DEBUG);
  }

  /**
   * (non-PHPdoc)
   * @see sites/all/modules/contrib/migrate/includes/MigrateSource::valid()
   */
  public function valid() {
    $ret = isset($this->currentRow) && !$this->eof; // && $this->numProcessed < $this->count;
    1||watchdog('res', '@fun(@args): @tf', array('@fun' => __CLASS__ . '::' . __FUNCTION__, '@args' => implode(', ', func_get_args()), '@tf' => $ret ? 'True' : 'False'), WATCHDOG_DEBUG);
    return $ret;
  }
}

<?php
/**
 * @file
 *  The migrate remote static class.
 */
class MigrateRemote {
  protected static $migrateRemoteHelper;
  protected static $registered;

  /**
   * Returns the migrate remote helper instance.
   * @return MigrateRemoteHelper
   */
  public static function get() {
    if (!isset(self::$migrateRemoteHelper)) {
      $class = static::getMigrateRemoteHelperClass();
      self::$migrateRemoteHelper = new $class();
    }

    return self::$migrateRemoteHelper;
  }

  /**
   * Returns the class name of the migrate remote helper instance.
   * @return string
   */
  public static function getMigrateRemoteHelperClass() {
    return variable_get('migrate_remote_helper_class', 'MigrateRemoteHelper');
  }

  /**
   * Register as a shutdown function the MigrateRemote::executePing
   * for executing the pings only once in a request.
   */
  public static function remotePing() {
    if (!self::$registered) {
      // Register as a shutdown function to avoid pinging the remote sites
      // several times during the same request.
      drupal_register_shutdown_function('MigrateRemote::executePing');
      self::$registered = TRUE;
    }
  }

  /**
   * Wrapper function to call the ping execution.
   */
  public static function executePing() {
    return static::get()->executePing();
  }

  /**
   * Wrapper function to make the entity touch.
   * @param $entity_type
   * @param $entity
   * @param array $clients
   */
  public static function entityTouch($entity_type, $entity, $clients = array()) {
    return static::get()->entityTouch($entity_type, $entity, $clients);
  }

  /**
   * Wrapper function to make the entity touch by ids.
   * @param $entity_type
   * @param $ids
   * @param array $clients
   */
  public static function entityTouchByIds($entity_type, $ids, $clients = array()) {
    return static::get()->entityTouchByIds($entity_type, $ids, $clients);
  }

  /**
   * Wrapper function to add a migration to the ping parameters.
   * @param $migration
   */
  public static function addMigration($migration) {
    return static::get()->addMigration($migration);
  }

  /**
   * Wrapper function to add devices to the ping parameters.
   * @param $devices
   */
  public static function addDevices($devices) {
    return static::get()->addDevices($devices);
  }
}

/**
 *  Interface for the remote migrate helper classes.
 */

interface MigrateRemoteHelperInterface {
  function entityTouch($entity_type, $entity, $clients = array());
  function entityTouchByIds($entity_type, $ids, $clients = array());
  function executePing();
  function addMigration($migration);
  function addDevices($devices);
}
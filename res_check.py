# coding: utf-8
#
# Test client for Migrate Remote RES XML-RPC methods
#
# @copyright (c) 2011 OSInet
#
# @author Frédéric G. MARAND (fgm@osinet.fr)
#
# Licensed under the General Public License version 2 or later.

import xmlrpclib, yaml

#Port 80 is the default
server = xmlrpclib.ServerProxy("http://localhost/xmlrpc.php")

syo = server.system

helpList = syo.listMethods()
print "Method List"
print helpList
print

for fun in helpList:
  signatureList = syo.methodHelp(fun)
  print fun + ": " 
  if 0 != len(signatureList):
    print ('  ' + signatureList) 
  try:
    signature = syo.methodSignature(fun)
  except xmlrpclib.Fault:
    signature = []
  print '  ' + signature[0] + ' from ' + yaml.dump(signature[1:]) if 0 != len(signatureList) else ''

<?php
/**
 * @file
 *  The default migrate remote helper class.
 */

class MigrateRemoteHelper implements MigrateRemoteHelperInterface {
  /**
   * Touches the entities by the provided ids without loading them.
   * @param $entity_type
   * @param $ids
   * @param array $clients
   *  A list of client endpoints.
   */
  public function entityTouchByIds($entity_type, $ids, $clients = array()) {
    foreach ($ids as $id) {
      try {
        res_entity_touch_by_id($entity_type, $id);
      }
      catch (Exception $e) {
        watchdog('res', $e->getMessage(), WATCHDOG_NOTICE);
      }
    }
  }

  /**
   * Touches the provided entity.
   * @param $entity_type
   * @param $entity
   * @param array $clients
   */
  public function entityTouch($entity_type, $entity, $clients = array()) {
    try {
      res_entity_touch($entity_type, $entity);
    }
    catch (Exception $e) {
      watchdog('res', $e->getMessage(), WATCHDOG_NOTICE);
    }
  }

  /**
   * Callback function to execute the ping.
   */
  public function executePing() {
    $clients = db_select('res_client', 'rc',  array('fetch' => PDO::FETCH_ASSOC))
      ->fields('rc')
      ->condition('status', 1)
      ->execute();

    foreach ($clients as $client) {
      $sts = xmlrpc($client['endpoint'], array('migrateRemote.enqueueImport' => array('', array())));
      if (!$sts) {
        $message = xmlrpc_error_msg();
        drupal_set_message(t('Unable to ping client @client: @message', array('@client' => $client['base'], '@message' => $message)), 'warning');
        watchdog('migrate_remote', 'Unable to ping client @client: @message', array('@client' => $client['base'], '@message' => $message), WATCHDOG_WARNING);
      }
    }

    watchdog('migrate_remote', 'Pinged the remote sites', array(), WATCHDOG_NOTICE);
  }

  /**
   * Add a migration to the list, this list will be used when the devices
   * will be pinged.
   * @param $migration
   */
  public function addMigration($migration) {
    // Do nothing, this helper pings all of the devices every time for every
    // migration in the list.
  }

  /**
   * Add devices to the list of devices to be pinged.
   * @param $device_ids
   */
  public function addDevices($device_ids) {
    // Do nothing, this helper pings all of the devices every time for every
    // migration in the list.
  }
}